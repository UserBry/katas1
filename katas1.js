function oneThroughTwenty() {
    const numbers = [];

    // Your code goes below
    for (let counter = 1; counter <= 20; counter++)
    {
        numbers.push(counter);
    }
    return numbers;
}

function evensToTwenty() {
    const numbers = [];
    
    // Your code goes below
    for (let counter = 1; counter <= 20; counter++)
    {
        if(counter % 2 == 0)
        {
            numbers.push(counter);
        }
    }
    return numbers;
}

function oddsToTwenty() {
    const numbers = []
    
    // Your code goes below
    for (let counter = 1; counter <= 20; counter++)
    {
        if(counter % 2 == 1)
        {
            numbers.push(counter);
        }
    }
    return numbers;
}

//5 10 15 20 25   so ... 10 / 5 = 2    15 / 5 = 3    20 / 5 = 4  
function multiplesOfFive() {
    const numbers = [];
    
    // Your code goes below
    let counter = 5;
    while (counter <= 100)
    {
        numbers.push(counter);
        counter = counter + 5;
    }
    return numbers;
}

function squareNumbers() {
    const numbers = []

    // Your code goes below
    let counter = 1;
    for (let i = 1; i <= 10; i++)
    {
        counter = i * i;
        numbers.push(counter);
    }
    return numbers;
}

function countingBackwards() {
    const numbers = []
    
    // Your code goes below
    for (counter = 20; counter >= 1; counter--)
    {
        numbers.push(counter);
    }
    return numbers;
}

function evenNumbersBackwards() {
    const numbers = []
    
    // Your code goes below
    for (let counter = 20; counter >= 1; counter--)
    {
        if(counter % 2 == 0)
        {
            numbers.push(counter);
        }
    }
    return numbers;
}

function oddNumbersBackwards() {
    const numbers = []
    
    // Your code goes below
    for (let counter = 20; counter >= 1; counter--)
    {
        if(counter % 2 == 1)
        {
            numbers.push(counter);
        }
    }
    return numbers;
}

function multiplesOfFiveBackwards() {
    const numbers = []
    
    // Your code goes below
    let counter = 100;
    while (counter >= 1)
    {
        numbers.push(counter);
        counter = counter - 5;
    }
    return numbers;
}

function squareNumbersBackwards() {
    const numbers = []
    
    // Your code goes below
    let counter = 100;
    for(let i = 10; i >= 1; i--)
    {
        counter = i * i;
        numbers.push(counter);
    }
    return numbers;
}